from flask import Flask, render_template, redirect
import RPi.GPIO as GPIO
import time
import sys
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

app = Flask(__name__)

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
#LED
GPIO.setup(11,GPIO.OUT)
GPIO.output(11, GPIO.LOW)
#sensor
GPIO.setup(7, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

status = 'Tidak Ada Gas'

@app.route('/')
def index():
	return render_template('indeks.html', berita = status)

@app.route('/turnOn')
def turnOn():
	GPIO.output(11, GPIO.HIGH)
	global status
	status = "Ada Gas!!!"
	return redirect("/")

@app.route('/turnOff')
def turnOff():
	GPIO.output(11, GPIO.LOW)
	global status
	status = 'Tidak ada Gas'
	return redirect("/")

def action(pin):
	global status
	sendEmail()
	GPIO.output(11,GPIO.HIGH)
	return redirect("/")

def fungsi(pin):
	global status
	status = 'Ada Gas!!!'
	GPIO.output(11,GPIO.HIGH)
	return
    
def sendEmail():
	server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
	server.ehlo()

	server.login("rubemcerdas@gmail.com", "Rubem123")

	fromaddr = "rubemcerdas@gmail.com"
	toaddr = "barraza.ahmad@gmail.com"
	msg = "Alarm Menyala Bung"

	server.sendmail(fromaddr, toaddr, msg)
	server.close
	
GPIO.add_event_detect(7, GPIO.RISING, fungsi)
GPIO.add_event_callback(7, action)


if __name__ == '__main__':
	app.run(debug=True, host='0.0.0.0')
	try:
		while True:
			print(status)
			time.sleep(0.5)
	except KeyboardInterrupt:
		GPIO.cleanup()
		sys.exit()
